from django.urls import path
from .views import (
    create_project,
    project_list_view,
    show_project,
)


urlpatterns = [
    path("", project_list_view, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
