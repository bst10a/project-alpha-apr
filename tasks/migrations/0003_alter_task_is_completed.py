# Generated by Django 4.1.4 on 2022-12-07 00:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("tasks", "0002_alter_task_assignee"),
    ]

    operations = [
        migrations.AlterField(
            model_name="task",
            name="is_completed",
            field=models.BooleanField(verbose_name=False),
        ),
    ]
